class CreateVagasPretendidas < ActiveRecord::Migration[5.0]
  def change
    create_table :vagas_pretendidas do |t|
    	t.string 		:nome
    	t.text 			:descricao
    	t.boolean 		:ativo
    end
  end
end
