class CreateContatoAssunto < ActiveRecord::Migration[5.0]
  def change
    create_table :contato_assuntos do |t|
    	t.string 		:nome
    	t.text 			:descricao
    	t.boolean 		:ativo
    end
  end
end
