class CreateEmpreendimentos < ActiveRecord::Migration[5.0]
  def change
    create_table :empreendimentos do |t|
    	t.string :nome
    	t.string :descricao
    	t.string :incorporadora
    	t.string :cnpj
    	t.string :ie
    	t.string :email
        t.boolean :ativo, default: true
		#==========================================================================
    	t.string :dormitorios
    	t.string :salas
    	t.string :cozinhas
    	t.string :elevadores
    	t.string :suites
    	t.string :banheiros
    	t.string :garagens
    	t.string :unidades
    	t.string :area_total
    	t.string :area_util
    	t.string :area_apartamentos
    	t.string :valores
    	t.string :condominio
    	t.text 	 :tipologias
    	t.text 	 :proximidades
    	t.text 	 :observacoes
    	t.boolean :piscina
    	t.boolean :parque
    	t.boolean :mobiliado
    	t.boolean :wi_fi
    	t.boolean :academia
    	t.boolean :salao_festa
    	t.boolean :pronto
    	t.boolean :em_obra
    	t.boolean :na_planta

        t.json :fotos

        t.timestamps
		#==========================================================================
        t.references :construtora, index: true
    	t.references :categoria, index: true
    	t.references :endereco, index: true
    end
  end
end
