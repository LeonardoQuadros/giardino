class CreateContatos < ActiveRecord::Migration[5.0]
  def change
    create_table :contatos do |t|
    	t.string 					:nome
    	t.string 					:email
    	t.string 					:telefone
        t.string                    :celular
        t.string                    :cidade
        t.boolean                   :sac, default: false
        t.boolean                   :trabalhe_conosco, default: false
        t.boolean                   :dicas, default: false
        t.boolean                   :lead, default: false
        t.text                      :mensagem
        t.json                      :curriculo
    	#===============================================================
        t.references :empreendimento, index: true
        t.references :contato_assunto, index: true
    	t.references :vagas_pretendidas, index: true
    	t.references :endereco, index: true
    end
  end
end
