class DeviseCreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip

      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at
      t.boolean :ativo, default: true

      #pessoa
      t.string      :apelido
      t.string      :cpf
      t.string      :cnpj
      t.string      :rg
      t.string      :telefone
      t.string      :celular
      t.date        :data_nascimento
      t.string      :nacionalidade
      t.string      :profissão
      t.decimal     :renda_familiar
      t.boolean     :flg_casado
      t.boolean     :flg_pessoa_fisica
      t.string      :regime_bens
      t.boolean     :flg_aposentado
      t.references                :endereco, index: true

        #rede social
        t.string :name
        t.string :image
        t.string :provider
        t.string :uid
        t.string :location
        t.string :sex

        #admin
        t.boolean :flg_admin, default: false

        #funcionario
        t.boolean :flg_funcionario, default: false
        
        #funcionario
        t.boolean :flg_cliente, default: false

      t.timestamps null: false
    end

    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    # add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true
  end
end
