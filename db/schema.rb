# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180122131108) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categorias", force: :cascade do |t|
    t.string  "nome"
    t.text    "descricao"
    t.boolean "ativo"
  end

  create_table "contato_assuntos", force: :cascade do |t|
    t.string  "nome"
    t.text    "descricao"
    t.boolean "ativo"
  end

  create_table "contatos", force: :cascade do |t|
    t.string  "nome"
    t.string  "email"
    t.string  "telefone"
    t.string  "celular"
    t.string  "cidade"
    t.boolean "sac",                  default: false
    t.boolean "trabalhe_conosco",     default: false
    t.boolean "dicas",                default: false
    t.boolean "lead",                 default: false
    t.text    "mensagem"
    t.json    "curriculo"
    t.integer "empreendimento_id"
    t.integer "contato_assunto_id"
    t.integer "vagas_pretendidas_id"
    t.integer "endereco_id"
    t.index ["contato_assunto_id"], name: "index_contatos_on_contato_assunto_id", using: :btree
    t.index ["empreendimento_id"], name: "index_contatos_on_empreendimento_id", using: :btree
    t.index ["endereco_id"], name: "index_contatos_on_endereco_id", using: :btree
    t.index ["vagas_pretendidas_id"], name: "index_contatos_on_vagas_pretendidas_id", using: :btree
  end

  create_table "empreendimentos", force: :cascade do |t|
    t.string   "nome"
    t.string   "descricao"
    t.string   "incorporadora"
    t.string   "cnpj"
    t.string   "ie"
    t.string   "email"
    t.boolean  "ativo",             default: true
    t.string   "dormitorios"
    t.string   "salas"
    t.string   "cozinhas"
    t.string   "elevadores"
    t.string   "suites"
    t.string   "banheiros"
    t.string   "garagens"
    t.string   "unidades"
    t.string   "area_total"
    t.string   "area_util"
    t.string   "area_apartamentos"
    t.string   "valores"
    t.string   "condominio"
    t.text     "tipologias"
    t.text     "proximidades"
    t.text     "observacoes"
    t.boolean  "piscina"
    t.boolean  "parque"
    t.boolean  "mobiliado"
    t.boolean  "wi_fi"
    t.boolean  "academia"
    t.boolean  "salao_festa"
    t.boolean  "pronto"
    t.boolean  "em_obra"
    t.boolean  "na_planta"
    t.json     "fotos"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "construtora_id"
    t.integer  "categoria_id"
    t.integer  "endereco_id"
    t.index ["categoria_id"], name: "index_empreendimentos_on_categoria_id", using: :btree
    t.index ["construtora_id"], name: "index_empreendimentos_on_construtora_id", using: :btree
    t.index ["endereco_id"], name: "index_empreendimentos_on_endereco_id", using: :btree
  end

  create_table "enderecos", force: :cascade do |t|
    t.string   "rua"
    t.string   "numero"
    t.string   "complemento"
    t.string   "bairro"
    t.string   "cep"
    t.string   "cidade"
    t.string   "estado"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "ativo",                  default: true
    t.string   "apelido"
    t.string   "cpf"
    t.string   "cnpj"
    t.string   "rg"
    t.string   "telefone"
    t.string   "celular"
    t.date     "data_nascimento"
    t.string   "nacionalidade"
    t.string   "profissão"
    t.decimal  "renda_familiar"
    t.boolean  "flg_casado"
    t.boolean  "flg_pessoa_fisica"
    t.string   "regime_bens"
    t.boolean  "flg_aposentado"
    t.string   "name"
    t.string   "image"
    t.string   "provider"
    t.string   "uid"
    t.string   "location"
    t.string   "sex"
    t.boolean  "flg_admin",              default: false
    t.boolean  "flg_funcionario",        default: false
    t.boolean  "flg_cliente",            default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "vagas_pretendidas", force: :cascade do |t|
    t.string  "nome"
    t.text    "descricao"
    t.boolean "ativo"
  end

end
