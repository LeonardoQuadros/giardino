class ContatosController < PublicController
	skip_before_filter :verify_authenticity_token  

	def index
		@contato = Contato.new
	end

	def new
		@contato = Contato.new
	end

	def create
		@contato = Contato.new(contato_params)

		respond_to do |format|
			@contato.save
			format.js
			format.json
		end

	end




    # Only allow a trusted parameter "white list" through.
    def contato_params
    	params.require(:contato).permit(:nome, :email, :celular, :telefone, :empreendimento_id, :contato_assunto_id, :vagas_pretendidas_id,
    									 :mensagem, :sac, :lead, :trabalhe_conosco, :dicas, :curriculo, :cidade)
    end
end

#Rails.logger.debug(" ==>  Contato: " + "#{contato.name}")
