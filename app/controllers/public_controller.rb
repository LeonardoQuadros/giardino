class PublicController < ApplicationController
  layout 'publico'

  before_filter :seo



  private

    # Cria as meta tags.
    def seo
      set_meta_tags description:  "Apartamentos com subsídios do governo federal através do programa minha casa minha vida, Construtora Giardino sinônimo de qualidade e segurança em São Carlos .",
                    title:        "Construtora Giardino",
                    keywords:     "atendimento, prédios, apartamento, satisfação, qualidade, complano, construtora, construções, construtoras, apartamentos, prédios, São Carlos, São Carlos - SP, São Paulo, apto, SP, casa própria, saia do aluguel, minha casa, minha vida, meu apê, construção civil, financiamento, meu sonho, realização, empreendimentos",
                    author:       'Construtora Giardino',
                    generator:    'Construtora Giardino',
                    charset:      "utf-8",
                    og: {
                      site_name:    "Giardino",
                      title:        "Construtora Giardino",
                      type:         'article',
                      description:  "Apartamentos com subsídios do governo federal através do programa minha casa minha vida, Construtora Giardino sinônimo de qualidade e segurança em São Carlos .",
                      url:          root_url[0..-2],
                      image:        root_url[0..-2] + ActionController::Base.helpers.image_url('logo.png')
                    }
    end
end
