class EnderecosController < PublicController

  def att_endereco

    begin
  	@endereco = nil
	@endereco = Correios::CEP::AddressFinder.get("#{params[:cep]}") unless params[:cep].blank?
    Rails.logger.info { @endereco }

    rescue Exception => e
      @erro = e
    end


    respond_to do |format|
      format.js
    end
  end

end
