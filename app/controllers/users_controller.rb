class UsersController < PublicController

	def novo_contato
		@user = User.new(user_params)

		user = User.where(email: params[:user][:email]).first unless params[:user][:email].blank?
		
	    respond_to do |format|

			if user.blank? or params[:user][:celular].blank? or params[:user][:name].blank?
				@user.save
			else
				@comprador = Comprador.new(user_params[:comprador_attributes])
				@comprador.user_id = user.id
				user.update(celular: params[:user][:celular])
				@comprador.save
			end
			format.js
			format.html
	    end

	end




    # Only allow a trusted parameter "white list" through.
    def user_params
    	params.require(:user).permit(:name, :email, :password, :telefone, :celular,
    		comprador_attributes: [:id, :mensagem, :produto_id, :agencia_id])
    end
end

#Rails.logger.debug(" ==>  User: " + "#{user.name}")
