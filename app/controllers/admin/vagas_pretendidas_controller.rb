class Admin::VagasPretendidasController < Admin::AdminController

  before_action :set_vagas_pretendidas, only: [:show, :edit, :update, :destroy]

  # GET /vagas_pretendidas
  def index
    conditions = []
    conditions << "vagas_pretendidas.id = #{params[:id]}" unless params[:id].blank?
    conditions << "UPPER(vagas_pretendidas.nome) LIKE '%#{params[:nome]}%'" unless params[:nome].blank?

    unless params[:titulo].blank?
      conditions << "UPPER(empreendimentos.titulo) LIKE '%#{params[:titulo]}%'"
      @vagas_pretendidas = VagasPretendidas.joins(:empreendimentos).where(conditions.join(' AND '))
    else
      @vagas_pretendidas = VagasPretendidas.where(conditions.join(' AND '))
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /vagas_pretendidas/1
  def show
    render layout: false
  end

  def new
    @vagas_pretendidas = VagasPretendidas.new
  end

  def edit
  end

  # PATCH/PUT /users/1
  def update
      if @vagas_pretendidas.update(vagas_pretendidas_params)
        redirect_to request.referrer, notice: "Vagas atualizada!"
      else
        render :edit
      end
  end

  def create
    @vagas_pretendidas = VagasPretendidas.new(vagas_pretendidas_params)

      if @vagas_pretendidas.save
        redirect_to request.referrer, notice: "Vagas Criada!"
      else
        render :new
      end
  end

  def destroy
    @vagas_pretendidas.destroy
    
    redirect_to request.referrer, notice: "Vagas Deletada!"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vagas_pretendidas
      @vagas_pretendidas = VagasPretendidas.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def vagas_pretendidas_params
      params.require(:vagas_pretendidas).permit(:nome, :descricao, :ativo)
    end


end
