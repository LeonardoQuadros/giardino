class Admin::EmpreendimentosController < Admin::AdminController

  before_action :set_empreendimento, only: [:show, :edit, :update, :destroy, :deletar_foto]

  # GET /empreendimentos
  def index
    conditions = []
    conditions << "empreendimentos.id = #{params[:id]}" unless params[:id].blank?
    conditions << "UPPER(empreendimentos.nome) LIKE '%#{params[:nome]}%'" unless params[:nome].blank?

    @empreendimentos = Empreendimento.where(conditions.join(' AND '))

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /empreendimentos/1
  def show
    render layout: false
  end

  def new
    @empreendimento = Empreendimento.new
    @empreendimento.endereco = Endereco.new
  end

  def edit
    @empreendimento.endereco = Endereco.new if @empreendimento.endereco.blank?
  end

  # PATCH/PUT /users/1
  def update
      if @empreendimento.update(empreendimento_params)
        if @empreendimento.endereco.blank?
          redirect_to edit_admin_empreendimento_path(@empreendimento), notice: "Empreendimento Criado! Adicione o ENDEREÇO."
        elsif @empreendimento.atributos_size < 3
          redirect_to edit_admin_empreendimento_path(@empreendimento), notice: "Empreendimento Atualizado! Adicione informações dos apartamentos."
        elsif @empreendimento.fotos.blank?
          redirect_to edit_admin_empreendimento_path(@empreendimento), notice: "Empreendimento Atualizado! Adicione as FOTOS."
        else
          redirect_to admin_empreendimentos_path, notice: "Empreendimento Finalizado!"
        end
      else
        render :edit
      end
  end

  def create
    @empreendimento = Empreendimento.new(empreendimento_params)

      if @empreendimento.save
        if @empreendimento.endereco.blank?
          redirect_to edit_admin_empreendimento_path(@empreendimento), notice: "Empreendimento Criado! Adicione o ENDEREÇO."
        elsif @empreendimento.atributos_size < 3
          redirect_to edit_admin_empreendimento_path(@empreendimento), notice: "Empreendimento Atualizado! Adicione informações dos apartamentos."
        elsif @empreendimento.fotos.blank?
          redirect_to edit_admin_empreendimento_path(@empreendimento), notice: "Empreendimento Atualizado! Adicione as FOTOS."
        else
          redirect_to admin_empreendimentos_path, notice: "Empreendimento Finalizado!"
        end
      else
        @empreendimento.endereco = Endereco.new if @empreendimento.endereco.blank?
        render :new
      end
  end

  def destroy
    @empreendimento.destroy

    redirect_to request.referrer, notice: "Empreendimento Deletado!"
  end

  def deletar_foto

    @empreendimento.fotos[params[:index].to_i].try(:remove!) # delete image from S3
    @empreendimento['fotos'].delete_at(params[:index].to_i) # remove from fotos array
    if @empreendimento.save
      @empreendimento.reload # if you need to reference the new set of fotos
      redirect_to request.referrer, notice: "Foto Deletada!"
    else
      redirect_to request.referrer, notice: "Não foi possível deletar esta foto!"
    end

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_empreendimento
      @empreendimento = Empreendimento.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def empreendimento_params
      params.require(:empreendimento).permit(:nome, :descricao, :incorporadora, :cnpj, :ie, :email, :ativo, :dormitorios, :salas, :categoria_id, {fotos: []},
                                              :cozinhas, :elevadores, :suites, :banheiros, :garagens, :unidades, :area_total, :area_util, :area_apartamentos, 
                                              :valores, :condominio, :tipologias, :proximidades, :observacoes, :piscina, :parque, :mobiliado, :wi_fi, 
                                              :academia, :salao_festa, :pronto, :em_obra, :na_planta,
                                              endereco_attributes: [:id, :cep, :rua, :bairro, :cidade, :estado, :numero, :complemento])
    end


end
