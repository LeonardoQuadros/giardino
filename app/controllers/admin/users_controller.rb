class Admin::UsersController < Admin::AdminController

  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  def index
    conditions = []
    conditions << "users.id = #{params[:id]}" unless params[:id].blank?
    conditions << "UPPER(users.name) LIKE '%#{params[:name]}%'" unless params[:name].blank?
    conditions << "UPPER(users.email) LIKE '%#{params[:email]}%'" unless params[:email].blank?

    @users = User.where(conditions.join(' AND '))

    # Mensagem para busca vazia
    if params[:commit] == t('helpers.links.search')
      if @users.count.zero?
        @system_notice = I18n.t('action_controller.nao_localizadas', model: 'Users')
        @type = 'warning'
      end
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /users/1
  def show
    render layout: false
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    @user.admin = Admin.new if @user.admin.blank?
  end

  # user /users
  def create
    @user = User.new(user_params)

      if @user.save
        redirect_to request.referrer, notice: "User Criado!"
      else
        render :new
      end
  end

  # PATCH/PUT /users/1
  def update
      if @user.update(user_params)
        redirect_to admin_users_path
      else
        render :edit
      end
  end

  # DELETE /users/1
  def destroy
    @user.destroy

    redirect_to request.referrer, notice: "User Deletado!"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :description, :email, :password, :password_confirmation, :current_password, :image, :flg_admin, :flg_corretor, :flg_vendedor,
                                   :apelido, :cpf, :cnpj, :rg, :telefone, :celular,
                                   :data_nascimento, :nacionalidade, :profissão, :renda_familiar, :sexo, :flg_casado, :flg_pessoa_fisica,
                                   :regime_bens, :flg_aposentado)
    end

end
