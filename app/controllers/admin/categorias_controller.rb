class Admin::CategoriasController < Admin::AdminController

  before_action :set_categoria, only: [:show, :edit, :update]

  # GET /categorias
  def index
    conditions = []
    conditions << "categorias.id = #{params[:id]}" unless params[:id].blank?
    conditions << "UPPER(categorias.nome) LIKE '%#{params[:nome]}%'" unless params[:nome].blank?

    unless params[:titulo].blank?
      conditions << "UPPER(empreendimentos.titulo) LIKE '%#{params[:titulo]}%'"
      @categorias = Categoria.joins(:empreendimentos).where(conditions.join(' AND '))
    else
      @categorias = Categoria.where(conditions.join(' AND '))
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /categorias/1
  def show
    render layout: false
  end

  def new
    @categoria = Categoria.new
  end

  def edit
  end

  # PATCH/PUT /users/1
  def update
      if @categoria.update(categoria_params)
        redirect_to request.referrer, notice: "Categoria atualizada!"
      else
        render :edit
      end
  end

  def create
    @categoria = Categoria.new(categoria_params)

      if @categoria.save
        redirect_to request.referrer, notice: "Categoria Criada!"
      else
        render :new
      end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categoria
      @categoria = Categoria.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def categoria_params
      params.require(:categoria).permit(:nome, :descricao, :ativo)
    end


end
