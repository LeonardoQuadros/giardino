class Admin::AdminController < ApplicationController
	
  before_action :authenticate_user!
  before_filter :seo
  
  layout 'admin'




  private

    # Cria as meta tags.
    def seo
      set_meta_tags description:  "Backend - Construtora Giardino.",
                    title:        "Construtora Giardino",
                    author:       'Construtora Giardino',
                    generator:    'Construtora Giardino',
                    charset:      "utf-8",
                    og: {
                      site_name:    "Construtora Giardino",
                      title:        "Construtora Giardino",
                      type:         'article',
                      description:  "Backend - Construtora Giardino.",
                      url:          root_url[0..-2],
                      image:        root_url[0..-2] + ActionController::Base.helpers.image_url('logo.png')
                    }
    end
  
end
