class Admin::ContatosController < Admin::AdminController

  before_action :set_contato, only: [:show, :edit, :update, :destroy]

  # GET /contatos
  def index
    conditions = []
    conditions << "contatos.id = #{params[:id]}" unless params[:id].blank?
    conditions << "UPPER(contatos.nome) LIKE '%#{params[:name]}%'" unless params[:name].blank?
    conditions << "UPPER(contatos.email) LIKE '%#{params[:email]}%'" unless params[:email].blank?
    conditions << "contatos.sac = '#{true}'" if params[:sac] == "true"
    conditions << "contatos.trabalhe_conosco = '#{true}'" if params[:trabalhe_conosco] == "true"
    conditions << "contatos.dicas = '#{true}'" if params[:dicas] == "true"

    @contatos = Contato.where(conditions.join(' AND '))

    # Mensagem para busca vazia
    if params[:commit] == t('helpers.links.search')
      if @contatos.count.zero?
        @system_notice = I18n.t('action_controller.nao_localizadas', model: 'Contatos')
        @type = 'warning'
      end
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /contatos/1
  def show
    render layout: false
  end

  # GET /contatos/new
  def new
    @contato = Contato.new
  end

  # GET /contatos/1/edit
  def edit
    @contato.admin = Admin.new if @contato.admin.blank?
  end

  # contato /contatos
  def create
    @contato = Contato.new(contato_params)

      if @contato.save
        redirect_to request.referrer, notice: "Contato Criado!"
      else
        render :new
      end
  end

  # PATCH/PUT /contatos/1
  def update
      if @contato.update(contato_params)
        redirect_to admin_contatos_path
      else
        render :edit
      end
  end

  # DELETE /contatos/1
  def destroy
    @contato.destroy

    redirect_to request.referrer, notice: "Contato Deletado!"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contato
      @contato = Contato.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contato_params
      params.require(:contato).permit(:name, :description, :email, :password, :password_confirmation, :current_password, :image, :flg_admin, :flg_corretor, :flg_vendedor,
                                   :apelido, :cpf, :cnpj, :rg, :telefone, :celular,
                                   :data_nascimento, :nacionalidade, :profissão, :renda_familiar, :sexo, :flg_casado, :flg_pessoa_fisica,
                                   :regime_bens, :flg_aposentado)
    end

end
