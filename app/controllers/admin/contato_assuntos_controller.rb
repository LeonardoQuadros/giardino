class Admin::ContatoAssuntosController < Admin::AdminController

  before_action :set_contato_assunto, only: [:show, :edit, :update]

  # GET /contato_assuntos
  def index
    conditions = []
    conditions << "contato_assuntos.id = #{params[:id]}" unless params[:id].blank?
    conditions << "UPPER(contato_assuntos.nome) LIKE '%#{params[:nome]}%'" unless params[:nome].blank?

    unless params[:titulo].blank?
      conditions << "UPPER(empreendimentos.titulo) LIKE '%#{params[:titulo]}%'"
      @contato_assuntos = ContatoAssunto.joins(:empreendimentos).where(conditions.join(' AND '))
    else
      @contato_assuntos = ContatoAssunto.where(conditions.join(' AND '))
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /contato_assuntos/1
  def show
    render layout: false
  end

  def new
    @contato_assunto = ContatoAssunto.new
  end

  def edit
  end

  # PATCH/PUT /users/1
  def update
      if @contato_assunto.update(contato_assunto_params)
        redirect_to request.referrer, notice: "Assunto atualizada!"
      else
        render :edit
      end
  end

  def create
    @contato_assunto = ContatoAssunto.new(contato_assunto_params)

      if @contato_assunto.save
        redirect_to request.referrer, notice: "Assunto Criada!"
      else
        render :new
      end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contato_assunto
      @contato_assunto = ContatoAssunto.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contato_assunto_params
      params.require(:contato_assunto).permit(:nome, :descricao, :ativo)
    end


end
