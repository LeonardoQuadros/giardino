class FotoUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :file
 
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

 
  #version :large do
  #  process resize_to_limit: [800, 800]
  #end


end