class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  validates :cpf, presence: true, :if => :eh_pessoa_fisica
  validates :cnpj, presence: true, :if => :eh_pessoa_juridica

  belongs_to :endereco, dependent: :destroy
  accepts_nested_attributes_for :endereco,  allow_destroy: true

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.name = auth.info.name
      user.password = Devise.friendly_token[0,20]
      user.image = auth.info.image
    end
  end


  def eh_pessoa_fisica
    self.flg_pessoa_fisica == true
  end

  def eh_pessoa_juridica
    self.flg_pessoa_fisica == false
  end
  
  def admin?
    return self.flg_admin
  end

  def fone
    fone = []
    fone << self.telefone unless self.telefone.blank?
    fone << self.celular unless self.celular.blank?
    return fone
  end

  def image_to
    if self.image.blank?
      "/images/profile/default.png"
    else
      self.image
    end
  end


end