class ContatoAssunto < ApplicationRecord

  validates :nome, presence: true

  has_many :contatos

end