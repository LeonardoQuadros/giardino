# -*- encoding : utf-8 -*-
class Empreendimento < ActiveRecord::Base

  validates :nome, :categoria, presence: true

  validates_uniqueness_of :nome, :cnpj

  mount_uploaders :fotos, FotoUploader

  belongs_to :endereco, dependent: :destroy
  accepts_nested_attributes_for :endereco,  allow_destroy: true, reject_if: lambda { |a| a[:cidade].blank? and a[:rua].blank? and a[:cep].blank? }

  belongs_to :categoria

  scope :ordenados, -> {order('empreendimentos.created_at DESC')}

  def self.to_select
    Empreendimento.ordenados.collect { |u| [u.nome, u.id] }
  end


  def atributos_size
    quantidade = 0
    quantidade += 1 unless self.dormitorios.blank? or self.dormitorios == 0
    quantidade += 1 unless self.salas.blank? or self.salas == 0
    quantidade += 1 unless self.cozinhas.blank? or self.cozinhas == 0
    quantidade += 1 unless self.banheiros.blank? or self.banheiros == 0
    quantidade += 1 unless self.garagens.blank? or self.garagens == 0
    quantidade += 1 unless self.suites.blank? or self.suites == 0
    quantidade += 1 unless self.unidades.blank? or self.unidades == 0
    quantidade += 1 unless self.area_total.blank? or self.area_total == 0
    quantidade += 1 unless self.area_util.blank? or self.area_util == 0
    quantidade += 1 unless self.area_apartamentos.blank? or self.area_apartamentos == 0
    quantidade += 1 unless self.elevadores.blank? or self.elevadores == 0
    quantidade += 1 unless self.tipologias.blank?
    return quantidade
  end

end
