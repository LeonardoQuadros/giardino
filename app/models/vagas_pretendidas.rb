class VagasPretendidas < ApplicationRecord

  validates :nome, presence: true

  has_many :contatos

  def self.to_select
  	VagasPretendidas.all.collect { |c| [c.nome, c.id] }
  end

end