class Endereco < ApplicationRecord

  validates :cidade, :rua, :bairro, :cep, presence: true


  def self.to_select
  	retorno = []
  	Endereco.all.select{|n| retorno += ["#{n.bairro}"] 	unless retorno.include?"#{n.bairro}"   }
  	Endereco.all.select{|n| retorno += ["#{n.cidade}"] 	unless retorno.include?"#{n.cidade}"  }
  	Endereco.all.select{|n| retorno += ["#{n.rua}"] 	unless retorno.include?"#{n.rua}"   }
  	return retorno
  end

  def completo
  	retorno = ""
  	retorno += "#{self.bairro}" unless self.bairro.blank?
  	retorno += ", #{self.cidade}" unless self.cidade.blank?
  	retorno
  end

end