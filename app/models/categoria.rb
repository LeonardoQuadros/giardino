class Categoria < ApplicationRecord

  validates :nome, presence: true

  has_many :empreendimentos

  def self.to_select
  	Categoria.all.collect { |c| [c.nome, c.id] }
  end

end