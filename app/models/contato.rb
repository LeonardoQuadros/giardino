# -*- encoding : utf-8 -*-
class Contato < ActiveRecord::Base

  validates :nome, :email, :celular, presence: true

  validates :empreendimento, :contato_assunto, :mensagem, presence: true, :if => :eh_sac

  validates :vagas_pretendidas, :curriculo, presence: true, :if => :eh_trabalhe_conosco

  mount_uploader :curriculo, CurriculoUploader

  belongs_to :empreendimento
  belongs_to :contato_assunto
  belongs_to :vagas_pretendidas


  def eh_sac
    self.sac
  end

  def eh_trabalhe_conosco
    self.trabalhe_conosco
  end

  def phones
  	retorno = []
  	retorno << self.celular unless self.celular.blank?
  	retorno << self.telefone unless self.telefone.blank?
  	return retorno
  end

end
