# -*- encoding: utf-8 -*-
# stub: correios-cep 0.6.5 ruby lib

Gem::Specification.new do |s|
  s.name = "correios-cep"
  s.version = "0.6.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Prodis a.k.a. Fernando Hamasaki de Amorim"]
  s.date = "2017-12-04"
  s.description = "Correios CEP gem finds updated Brazilian addresses by zipcode, directly from Correios database. No HTML parsers."
  s.email = "prodis@gmail.com"
  s.executables = ["console"]
  s.files = ["bin/console"]
  s.homepage = "https://github.com/prodis/correios-cep"
  s.licenses = ["MIT"]
  s.required_ruby_version = Gem::Requirement.new(">= 2.0.0")
  s.rubygems_version = "2.5.1"
  s.summary = "Correios CEP gem finds updated Brazilian addresses by zipcode, directly from Correios database. No HTML parsers."

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<log-me>, ["~> 0.0.10"])
      s.add_runtime_dependency(%q<ox>, [">= 2.8.2", "~> 2.8"])
      s.add_development_dependency(%q<coveralls>, ["~> 0.8.21"])
      s.add_development_dependency(%q<pry>, ["~> 0.11.3"])
      s.add_development_dependency(%q<rake>, ["~> 12.3"])
      s.add_development_dependency(%q<rspec>, ["~> 3.7"])
      s.add_development_dependency(%q<vcr>, ["~> 4.0"])
      s.add_development_dependency(%q<webmock>, [">= 3.1.1", "~> 3.1"])
    else
      s.add_dependency(%q<log-me>, ["~> 0.0.10"])
      s.add_dependency(%q<ox>, [">= 2.8.2", "~> 2.8"])
      s.add_dependency(%q<coveralls>, ["~> 0.8.21"])
      s.add_dependency(%q<pry>, ["~> 0.11.3"])
      s.add_dependency(%q<rake>, ["~> 12.3"])
      s.add_dependency(%q<rspec>, ["~> 3.7"])
      s.add_dependency(%q<vcr>, ["~> 4.0"])
      s.add_dependency(%q<webmock>, [">= 3.1.1", "~> 3.1"])
    end
  else
    s.add_dependency(%q<log-me>, ["~> 0.0.10"])
    s.add_dependency(%q<ox>, [">= 2.8.2", "~> 2.8"])
    s.add_dependency(%q<coveralls>, ["~> 0.8.21"])
    s.add_dependency(%q<pry>, ["~> 0.11.3"])
    s.add_dependency(%q<rake>, ["~> 12.3"])
    s.add_dependency(%q<rspec>, ["~> 3.7"])
    s.add_dependency(%q<vcr>, ["~> 4.0"])
    s.add_dependency(%q<webmock>, [">= 3.1.1", "~> 3.1"])
  end
end
