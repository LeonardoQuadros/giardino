Rails.application.routes.draw do

#=============================================================================
# ================================ ADMIN =====================================
#=============================================================================
  namespace :admin do

    controller :users do
    end
    resources :users

    controller :empreendimentos do
      post '/empreendimentos/deletar_foto/:id' => :deletar_foto, as: :empreendimentos_deletar_foto
    end
    resources :empreendimentos

    controller :categorias do
    end
    resources :categorias

    controller :contatos do
    end
    resources :contatos

    controller :contato_assuntos do
    end
    resources :contato_assuntos

    controller :vagas_pretendidas do
    end
    resources :vagas_pretendidas

    controller :pages do
        get '/home' => :home
    end
    root 'pages#home'
  end

#=============================================================================
# ================================ PÚBLICO ===================================
#=============================================================================

  devise_for :users, :controllers => { registrations: 'registrations',
                                        :omniauth_callbacks => "callbacks" }



  authenticated :user do
    root 'admin/pages#home', as: :authenticated_root
  end

  controller :users do
    post "users/novo_contato" => :novo_contato
  end
  resources :users, only: [:new, :create]

  controller :empreendimentos do
  end
  resources :empreendimentos, only: [:index, :show]

  controller :contatos do
  end
  resources :contatos, only: [:index, :new, :create]

  controller :enderecos do
    post "enderecos/att_endereco" => :att_endereco
  end

  controller :pages do
    get '/home' => :home
    get '/sac' => :sac
    get '/trabalhe_conosco' => :trabalhe_conosco
    get '/dicas' => :dicas
    get '/minha_casa_minha_vida' => :minha_casa_minha_vida
  end 

  root 'pages#home'

end